+++
title = "About Grace"
date = "2019-01-22"
sidemenu = "true"
description = "Welcome to my site!"
+++

I am a current Master's of Public Administration candidate at OHIO University's Voinovich School of Leadership and Public Affairs.Through my work and educational experiences I have come to realize the importance of integrating data management, analytics, and the visual representation of data into my everyday work and research. Not only do I view data as important, but I am inspired by the critical thinking and continuous learning opportunities that are involved in the field of research analytics. Over the course of my master's program, I have been working on gaining the skills to tell stories and better understand the world around me. This blog is a space to not only share my  technical skill progress, but to also explore social issues that are interesting to me, and that I hope will be interesting to you!   

